# vivaldi-widevine

A browser plugin designed for the viewing of premium video content, standalone for vivaldi

https://www.widevine.com/

How to clone this repo:

```
git clone https://gitlab.com/reborn-os-team/rebornos-packages/browsers/vivaldi-widevine.git
```

